from django import forms
from django.contrib.auth.forms import UserCreationForm

class LoginForm(forms.Form):
	username = forms.CharField(label="Username", max_length=20)
	password = forms.CharField(label="Password", max_length=20)

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)
	status = forms.CharField(label='Status', max_length=50)

class AddEventForm(forms.Form):
	event_name = forms.CharField(label="Event Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)

class UpdateUserForm(forms.Form):
    first_name = forms.CharField(max_length=30, required=False)
    last_name = forms.CharField(max_length=30, required=False)
    password = forms.CharField(max_length=128, widget=forms.PasswordInput, required=False)

class RegisterForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Enter a valid email address.')

    class Meta:
        model = super
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']
